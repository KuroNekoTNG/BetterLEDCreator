using System;
using System.Threading;
using System.Threading.Tasks;
using Avalonia.Controls;
using Avalonia.Input;
using Avalonia.VisualTree;

namespace LedBadgeCreator.Views {
	public partial class MainWindow : Window {

		private bool isTracking;

		private Canvas  canvas;
		private Pointer pointer;

		public MainWindow() {
			InitializeComponent();

			canvas = this.Find<Canvas>( "Canvas" );
		}

		private void PointerClicked( object? sender, PointerPressedEventArgs e ) {
			isTracking = true;
			
			
		}

		private void PointerReleased( object? sender, PointerReleasedEventArgs e ) {

		}

		private void Moved( object? sender, PointerEventArgs e ) {

		}
	}
}
