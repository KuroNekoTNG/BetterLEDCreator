﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Controls.Primitives;
using Avalonia.Markup.Xaml;
using LEDBadgeCreator.Controls;

namespace LedBadgeCreator.Controls {
	public partial class LedGrid : UniformGrid {

		public LedGrid() : this( 11, 100 ) { }

		public LedGrid( int rows, int columns ) {
			InitializeComponent();

			Rows    = rows;
			Columns = columns;

			for ( var i = 0; i < Rows * Columns; ++i ) {
				Children.Add( new LedControl() );
			}
		}
	}
}
