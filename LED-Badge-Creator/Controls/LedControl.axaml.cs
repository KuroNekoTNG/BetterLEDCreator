﻿using System;
using System.Data;
using System.Drawing;
using System.Globalization;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Data.Converters;
using Avalonia.Media;
using Brushes = System.Drawing.Brushes;
using Color = System.Drawing.Color;
using Rectangle = Avalonia.Controls.Shapes.Rectangle;

namespace LEDBadgeCreator.Controls {
	public partial class LedControl : UserControl {

		public static StyledProperty<bool> IsOnProperty = AvaloniaProperty.Register<LedControl, bool>( nameof( IsOn ) );

		private static readonly IBrush OnBrush  = new SolidColorBrush( Avalonia.Media.Color.FromRgb( 255, 0, 0 ) );
		private static readonly IBrush OffBrush = new SolidColorBrush( Avalonia.Media.Color.FromRgb( 100, 100, 100 ) );

		public bool IsOn {
			get => GetValue( IsOnProperty );
			set => SetValue( IsOnProperty, value );
		}

		private Rectangle box;

		public LedControl() {
			InitializeComponent();

			box = this.FindControl<Rectangle>( "BoxFill" );

			IsOnProperty.Changed.Subscribe( v => box.Fill = ( IBrush? )( v.NewValue.Value ? OnBrush : OffBrush ) );

			IsOn = false;

			box.Fill = ( IBrush? )( IsOn ? OnBrush : OffBrush );
		}
	}

	public class LedColorConverter : IValueConverter {

		public object? Convert( object? value, Type targetType, object? parameter, CultureInfo culture ) {
			var isOn = ( bool )value;

			return isOn ? Color.Red : Color.Gray;
		}

		public object? ConvertBack( object? value, Type targetType, object? parameter, CultureInfo culture ) {
			throw new RowNotInTableException();
		}
	}
}
