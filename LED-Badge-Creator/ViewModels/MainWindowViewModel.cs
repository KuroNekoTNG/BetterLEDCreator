﻿namespace LedBadgeCreator.ViewModels {
	public class MainWindowViewModel : ViewModelBase {
		public int SelectedIndex {
			get;
			set;
		}
		
		public string Greeting => "Welcome to Avalonia!";
	}
}
