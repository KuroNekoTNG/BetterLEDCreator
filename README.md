# Better LED Badge Creator
## What is the purpose of this program?
This program is an attempted recreation of the horrible UIX of the default program you are given.
It is delivered via a Google Drive link, which doesn't feel safe at all, and isn't Open Source.
While it is free and doesn't trip any Anti-Malware, it still doesn't feel safe to use at all.

So this is a recreation to make it Free and Open Source under the GNU Public License.
Everyone is free to fork the code, or even contribute to it. Please note that this is, at this moment
a personal project and isn't anything too serious at the moment.

## How to compile
To compile this project, you only need .NET 6 or higher. You can also use an IDE that supports .NET 6 or higher.
To build from the command line, simple type out
```ps
dotnet build 
```
It is, however, recommended to use an IDE (Integrated Development Environment) to compile the code as it makes
it easier to identify the issues with compiling. I recommend JetBrains Rider, but Visual Studio can work as well.

## I wish to contribute!
Great! Please work on some code, the "Testing" project is simple to mess around in to figure out how the device works.
Please try to follow the MVVM (Model View ViewModel) structure as much as possible.

## Is this legal?
YES! This is very much legal as I am attempting a reverse engineering the communication to the device.
I am doing this via seeing what changes when sending data, I am also seeing how it communicates to the device through
USB sniffers. All of this in an attempt to figure out how the device works for making a better UI.

## Can I support somehow?
YES! You can support me via contributing code, I do not have a Patreon or Kofi. If you want to throw money at me, go
to my [Twitch](https://twitch.tv/kyuvulpes) and look for the StreamElements donation widget. Or you can go to my
[StreamElements profile](https://streamelements.com/kyuvulpes) page as well. Though I recommend only doing the donation
while I am streaming. Maybe if enough people support my work/streams would I potentially open up a Patreon.